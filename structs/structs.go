package structs

var Products []ProductAPI
var Order OrderAPI

var OrderId = ""

//var OrderId = "81db9f0e-a051-4bd5-abac-abb3a9b5d27c"

type ProductAPI struct {
	Id    int    `json:"id,omitempty"`
	Name  string `json:"name,omitempty"`
	Price string `json:"price,omitempty"`
}

type OrderAPI struct {
	Amount struct {
		Discount string `json:"discount,omitempty"`
		Paid     string `json:"paid,omitempty"`
		Returns  string `json:"returns,omitempty"`
		Total    string `json:"total,omitempty"`
	} `json:"amount,omitempty"`
	Id       string           `json:"id,omitempty"`
	Products []ProductInOrder `json:"products,omitempty"`
	Status   string           `json:"status,omitempty"`
}

type ProductInOrder struct {
	Id           string      `json:"id,omitempty"`
	Name         string      `json:"name,omitempty"`
	Price        string      `json:"price,omitempty"`
	ProductId    int         `json:"product_id,omitempty"`
	Quantity     int         `json:"quantity,omitempty"`
	ReplacedWith Replacement `json:"replaced_with,omitempty"`
}

type Replacement struct {
	ProductId int `json:"product_id,omitempty"`
	Quantity  int `json:"quantity,omitempty"`
}

type Quantity struct {
	Quantity int `json:"quantity"`
}
