"use strict";

const modal = document.querySelector(".modal");
const overlay = document.querySelector(".overlay");
const btnOpenModal = document.querySelector(".show-modal");
const btnCloseModal = document.querySelector(".close-modal");

function openModal() {
    modal.classList.remove("hidden");
    overlay.classList.remove("hidden");

    fetch('/open-order')
        .then(data => {
            return data.json();
        })
        .then(post => {
            let html = '<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet"><table class="table">';
            html += '<tr>';
            html += '<td><b>PRODUCTS</b></td>';
            html += '</tr>';
            html += '<tr>';
            html += '<td><b>NAME</b></td>';
            html += '<td><b>PRICE</b></td>';
            html += '<td><b>QUANTITY</b></td>';
            html += '</tr>';
            for (let i = 0; i < post.products.length; i++) {
                let quantityPlus = post.products[i].quantity + 1;
                let quantityMinus = post.products[i].quantity - 1;
                if (quantityMinus < 0) {
                    quantityMinus = 0
                }
                let addItem = "/update_quantity/" + post.products[i].id + "/" + quantityPlus;
                let removeItem = "/update_quantity/" + post.products[i].id + "/" + quantityMinus;

                html += '<tr>';
                html += '<td>' + post.products[i].name + '</td>';
                html += '<td>' + post.products[i].price + '</td>';
                html += '<td>' + post.products[i].quantity + '</td>';
                html += '<td><a href="' + removeItem + '"><span class="add-item-modal material-icons">remove</span></a></td>';
                html += '<td><a href="' + addItem + '"><span class="add-item-modal material-icons">add</span></a></td>';
                html += '</tr>';
            }
            html += '<tr>';
            html += '<td><b>DISCOUNT:</b></td>';
            html += '<td>' + post.amount.discount + '</td>';
            html += '</tr>';
            html += '<tr>';
            html += '<td><b>PAID:</b></td>';
            html += '<td>' + post.amount.paid + '</td>';
            html += '</tr>';
            html += '<tr>';
            html += '<td><b>RETURNS:</b></td>';
            html += '<td>' + post.amount.returns + '</td>';
            html += '</tr>';
            html += '<tr>';
            html += '<td><b>TOTAL:</b></td>';
            html += '<td>' + post.amount.total + '</td>';
            html += '</tr>';
            html += '</table>'
            display_cart.innerHTML = html;
        });
}

function  closeModal() {
    modal.classList.add("hidden");
    overlay.classList.add("hidden");
}

btnOpenModal.addEventListener("click", openModal);
btnCloseModal.addEventListener("click", closeModal);

// close cart when clicking outside of the cart modal
overlay.addEventListener("click", closeModal);

// close cart when pressing the escape key
document.addEventListener("keydown", function (e) {
    if (e.key === "Escape" && !modal.classList.contains("hidden")) {
        closeModal();
    }
});
