package main

import (
	"fmt"
	"github.com/gorilla/mux"
	"github.com/mjunolainen/simpleCart/api"
	"github.com/mjunolainen/simpleCart/config"
	"github.com/mjunolainen/simpleCart/structs"
)

const host = "localhost"

func main() {
	router := mux.NewRouter().StrictSlash(true)

	router.HandleFunc("/css/style.css", config.ServeFiles)
	router.HandleFunc("/js/script.js", config.ServeFiles)
	router.HandleFunc("/paid.html", config.ServeFiles)
	router.HandleFunc("/error.html", config.ServeFiles)

	router.HandleFunc("/", api.GetAllProducts)
	router.HandleFunc("/add_products/{productId}", api.AddProductToOrder)
	router.HandleFunc("/get_order_products", api.GetOrderProducts)
	router.HandleFunc("/update_quantity/{productId}/{quantity}", api.UpdateProductQuantity)
	router.HandleFunc("/open-order", api.OpenOrder)
	router.HandleFunc("/paid", api.UpdateOrderToPaid)

	//TODO - this doesn't work right now.
	router.HandleFunc("/replace_product/{oldProductId}/{newProductId}/{quantity}", api.AddReplacementProduct)

	// Create a new order before starting the server so that later you can just bring up the order
	// by using the existing order ID at structs.OrderID. The working hypothesis here is that I'm working with
	// just one order at any given time. Doing it like this page refresh doesn´t reload the function, and we're stuck
	// with just one order ID until the next restart of the application. Not the best way of doing things though...
	api.NewOrderOnAppLoad()
	fmt.Printf("Order id: %s \n", structs.OrderId)

	config.StartServer(config.GetConsoleInput(), host, router)
}
