package api

import (
	"bytes"
	"encoding/json"
	"fmt"
	"github.com/gorilla/mux"
	"github.com/mjunolainen/simpleCart/config"
	"github.com/mjunolainen/simpleCart/structs"
	"io/ioutil"
	"log"
	"net/http"
)

func UpdateProductQuantity(w http.ResponseWriter, r *http.Request) {

	if structs.Order.Status == "NEW" {
		params := mux.Vars(r)
		productId := params["productId"]
		updatedQuantityString := params["quantity"]

		updatedQuantity := config.AtoiNoErrors(updatedQuantityString)

		if productId == "" {
			fmt.Println("You don't have that item in your basket. But you can add one any time if you like!")
		} else {
			url := config.GetBaseUrl() + "api/orders/" + structs.OrderId + "/products/" + productId

			quantity := structs.Quantity{
				Quantity: updatedQuantity,
			}

			quantityBytes, err := json.Marshal(quantity)
			if err != nil {
				log.Fatalln("Error creating patch json", err)
				return
			}

			body := bytes.NewReader(quantityBytes)
			info, err := http.NewRequest(http.MethodPatch, url, body)
			info.Header.Set("Content-Type", "application/json; charset=utf-8")

			client := &http.Client{}
			resp, err := client.Do(info)
			if err != nil {
				log.Fatalln(err)
			} else {
				for i := range structs.Order.Products {
					if structs.Order.Products[i].Id == productId {
						structs.Order.Products[i].Quantity = updatedQuantity
					}
				}
			}
			defer resp.Body.Close()

			responseBytes, _ := ioutil.ReadAll(resp.Body)
			responseString := string(responseBytes)
			fmt.Printf("Request response: %s\n", responseString)

			http.Redirect(w, r, "/", http.StatusSeeOther)
		}
	} else {
		http.Redirect(w, r, "/error.html", http.StatusSeeOther)
	}
}
