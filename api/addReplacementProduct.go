package api

import (
	"bytes"
	"encoding/json"
	"fmt"
	"github.com/gorilla/mux"
	"github.com/mjunolainen/simpleCart/config"
	"github.com/mjunolainen/simpleCart/structs"
	"io/ioutil"
	"log"
	"net/http"
)

func AddReplacementProduct(w http.ResponseWriter, r *http.Request) {

	if structs.Order.Status == "NEW" {
		params := mux.Vars(r)
		oldProductId := params["oldProductId"]
		newProductIdString := params["newProductId"]
		newQuantityString := params["quantity"]

		newProductId := config.AtoiNoErrors(newProductIdString)

		for i := range structs.Order.Products {
			if structs.Order.Products[i].Name == oldProductId {
				oldProductId = structs.Order.Products[i].Id
			}
		}

		for i := range structs.Products {
			if structs.Products[i].Name == newProductIdString {
				newProductId = structs.Products[i].Id
			}
		}

		if oldProductId == "" || newProductId == 0 {
			fmt.Println("Either you are trying to replace an item you don't have in your basket or " +
				"you are trying to replace an item in your basket that is not in the range. " +
				"Either way it is a mess and you should try again.")
		} else {
			url := config.GetBaseUrl() + "api/orders/" + structs.OrderId + "/products/" + oldProductId

			// With the struct below the request response is OK, but it updates the quantity of the old product
			/*replacement := structs.Replacement{
				ProductId: newProductId,
				Quantity:  config.AtoiNoErrors(newQuantityString),
			}*/

			// With the struct below the request response is "Invalid parameters"
			replacement := structs.ProductInOrder{
				ReplacedWith: structs.Replacement{
					ProductId: newProductId,
					Quantity:  config.AtoiNoErrors(newQuantityString),
				},
			}
			fmt.Println(replacement)

			replacementBytes, err := json.Marshal(replacement)
			if err != nil {
				log.Fatalln("Error creating patch json", err.Error())
				return
			}
			fmt.Println(string(replacementBytes))

			body := bytes.NewReader(replacementBytes)
			info, err := http.NewRequest(http.MethodPatch, url, body)
			info.Header.Set("Content-Type", "application/json")

			client := &http.Client{}
			resp, err := client.Do(info)
			if err != nil {
				log.Fatalln(err)
			} else {
				for i := range structs.Order.Products {
					if structs.Order.Products[i].Id == oldProductId {
						structs.Order.Products[i].ReplacedWith.ProductId = replacement.ProductId
						structs.Order.Products[i].ReplacedWith.Quantity = replacement.Quantity
					}
				}
			}
			defer resp.Body.Close()

			responseBytes, _ := ioutil.ReadAll(resp.Body)
			responseString := string(responseBytes)
			fmt.Printf("Request response: %s\n", responseString)
		}

	} else {
		http.Redirect(w, r, "/error.html", http.StatusSeeOther)
	}
}
