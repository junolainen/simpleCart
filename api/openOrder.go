package api

import (
	"encoding/json"
	"fmt"
	"github.com/mjunolainen/simpleCart/config"
	"github.com/mjunolainen/simpleCart/structs"
	"io/ioutil"
	"log"
	"net/http"
)

func OpenOrder(w http.ResponseWriter, _ *http.Request) {
	url := config.GetBaseUrl() + "/api/orders/" + structs.OrderId

	info, err := http.Get(url)
	if err != nil {
		log.Fatalln(err)
	}
	defer info.Body.Close()

	data, err := ioutil.ReadAll(info.Body)
	if err != nil {
		log.Fatalln(err)
	}

	err = json.Unmarshal(data, &structs.Order)
	if err != nil {
		log.Fatalln(err)
	}
	fmt.Fprintf(w, "%s", data)
	return
}
