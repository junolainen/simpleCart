package api

import (
	"bytes"
	"encoding/json"
	"fmt"
	"github.com/gorilla/mux"
	"github.com/mjunolainen/simpleCart/config"
	"github.com/mjunolainen/simpleCart/structs"
	"io/ioutil"
	"log"
	"net/http"
)

func AddProductToOrder(w http.ResponseWriter, r *http.Request) {

	if structs.Order.Status == "NEW" {
		url := config.GetBaseUrl() + "api/orders/" + structs.OrderId + "/products"

		params := mux.Vars(r)
		inputProductID := params["productId"]

		// Convert product ID from string to int. Here a separate function instead of
		// strconv.Atoi() is used to discard a possible error message.
		inputID := config.AtoiNoErrors(inputProductID)

		var addProductArray []int
		addProductArray = append(addProductArray, inputID)

		for i := range structs.Products {
			if structs.Products[i].Id == inputID {

				productBytes, err := json.Marshal(addProductArray)
				if err != nil {
					log.Fatalln("Error marshaling json", err)
					return
				}

				info, err := http.NewRequest(http.MethodPost, url, bytes.NewBuffer(productBytes))
				info.Header.Set("Content-Type", "application/json")

				client := &http.Client{}
				resp, err := client.Do(info)
				if err != nil {
					log.Fatalln(err)
				}
				responseBytes, _ := ioutil.ReadAll(resp.Body)
				responseString := string(responseBytes)
				fmt.Printf("Request response: %s\n", responseString)
				resp.Body.Close()
			}
		}
		http.Redirect(w, r, "/", http.StatusSeeOther)
	} else {
		http.Redirect(w, r, "/error.html", http.StatusSeeOther)
	}
}
