package api

import (
	"encoding/json"
	"github.com/mjunolainen/simpleCart/config"
	"github.com/mjunolainen/simpleCart/structs"
	"html/template"
	"io/ioutil"
	"log"
	"net/http"
)

func GetAllProducts(w http.ResponseWriter, _ *http.Request) {

	url := config.GetBaseUrl() + "api/products"

	info, err := http.Get(url)
	if err != nil {
		log.Fatalln(err)
	}
	defer info.Body.Close()

	data, err := ioutil.ReadAll(info.Body)
	if err != nil {
		log.Fatalln(err)
	}

	err = json.Unmarshal(data, &structs.Products)
	if err != nil {
		log.Fatalln(err)
	}

	tmpl, err := template.New("index.html").ParseFiles("./templates/index.html")
	if err != nil {
		panic(err)
	}

	err = tmpl.Execute(w, structs.Products)
	if err != nil {
		panic(err)
	}
	return
}
