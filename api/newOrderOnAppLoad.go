package api

import (
	"encoding/json"
	"github.com/mjunolainen/simpleCart/config"
	"github.com/mjunolainen/simpleCart/structs"
	"io/ioutil"
	"log"
	"net/http"
)

func NewOrderOnAppLoad() {
	url := config.GetBaseUrl() + "api/orders"

	info, err := http.Post(url, "application/json", nil)
	if err != nil {
		log.Fatalln(err)
	}
	defer info.Body.Close()

	data, err := ioutil.ReadAll(info.Body)
	if err != nil {
		log.Fatalln(err)
	}

	err = json.Unmarshal(data, &structs.Order)
	if err != nil {
		log.Fatalln(err)
	}
	structs.OrderId = structs.Order.Id
	return
}
