package api

import (
	"bytes"
	"encoding/json"
	"fmt"
	"github.com/mjunolainen/simpleCart/config"
	"github.com/mjunolainen/simpleCart/structs"
	"io/ioutil"
	"log"
	"net/http"
)

func UpdateOrderToPaid(w http.ResponseWriter, r *http.Request) {

	if structs.Order.Status == "NEW" {
		url := config.GetBaseUrl() + "api/orders/" + structs.OrderId

		status := structs.OrderAPI{
			Status: "PAID",
		}

		statusBytes, err := json.Marshal(status)
		if err != nil {
			log.Fatalln("Error creating patch json", err)
			return
		}

		info, err := http.NewRequest(http.MethodPatch, url, bytes.NewBuffer(statusBytes))
		info.Header.Set("Content-Type", "application/json; charset=utf-8")

		client := &http.Client{}
		resp, err := client.Do(info)
		if err != nil {
			log.Fatalln(err)
		} else {
			structs.Order.Status = status.Status
		}
		defer resp.Body.Close()

		responseBytes, _ := ioutil.ReadAll(resp.Body)
		responseString := string(responseBytes)
		fmt.Printf("Request response: %s\n", responseString)

		http.Redirect(w, r, "/paid.html", http.StatusSeeOther)
	} else {
		http.Redirect(w, r, "/error.html", http.StatusSeeOther)
	}
}
