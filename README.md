# simpleCart

## What's going on?

Clone the repository and add an .env file in the root directory that contains the following:
  ```
BASE_URL=https://www.UrlForTheApiCalls.ee/
  ```
Run "go run ." in the console and follow the instructions.  

-----

There is a series of API endpoints doing their thing.
The description and where I'm at with them is below
along with end goals and other important stuff. 

* GET /api/products - list of all available products
    ```
  + Makes a request on page load and displays all available products on main page
  with an option to add them to the cart. 
    ```

* POST /api/orders - create new order
    ```
  + Creates a new order before starting the server. 
  Stores the order id into a variable. That allows to build the rest of the 
  functionality around that ID, as the working assumption is that the user will be operating
  with just a single order. The order will be recreated upon rerunning the application. 
   ```

* GET /api/orders/:order_id - get order details
    ```
  + Current order loads in the "show cart" modal
    ```

* PATCH /api/orders/:order_id - update order
    ```
    + Currently changes the order status to "paid" when clicking the button in the modal. 
    This function can't be reversed and after doing that you might as well go walk your dog. 
    ```

* GET /api/orders/:order_id/products - get order products
    ```  
  + At the moment this request is working, but sees no action. 
    ```

* POST /api/orders/:order_id/products - add products to order
    ```
  + Adds a single product to the order when clicking the trolley icon on the main page
  next to the product. 
      ```

* PATCH /api/orders/:order_id/products/:product_id - update product quantity
    ```
  + Quantity change happens with one step increments in the modal either by increasing or decreasing
  using this request. 
  //TODO Ideally the modal shouldn't close after adding or removing item from the cart.  
    ```

* PATCH /api/orders/:order_id/products/:product_id - add a replacement product
    ```
  //TODO this, well... for some reason the response is "Invalid parameters". Will need to 
  investigate further.
      ```

## Goals
1. Implement all API endpoints described above; + almost there. Except the last one. 
2. For `GET /api/products` endpoint keep products list exactly the same as is done  
in the reference API - same products with same attributes (even `id`); + DONE
3. Make it possible to configure listening port number of the application 
without any need to change the code; + DONE
4. Track time using Toggle and see if your estimations match the end time consumption. 
## Other
* backend is done using Go;
* Goal #3 - port preference is prompted in console and after entering a port the input is 
verified to make sure that it is a number and the chosen port is available. If it is not available
a random port is assigned and server is started on that port;

## ???
* 
