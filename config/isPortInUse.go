package config

import (
	"fmt"
	"net"
	"time"
)

func IsPortInUse(host string, port int) bool {
	timeout := 5 * time.Second
	target := fmt.Sprintf("%s:%d", host, port)
	conn, err := net.DialTimeout("tcp", target, timeout)
	if err != nil {
		return true
	}
	if conn != nil {
		conn.Close()
		return false
	}
	return true
}
