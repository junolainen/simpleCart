package config

import (
	"github.com/joho/godotenv"
	"log"
	"os"
	"path/filepath"
)

func GetBaseUrl() string {
	err := godotenv.Load(filepath.Join("./", ".env"))
	if err != nil {
		log.Fatalln(err)
	}
	url := os.Getenv("BASE_URL")

	return url
}
