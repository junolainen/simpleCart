package config

import (
	"fmt"
	"github.com/gorilla/mux"
	"log"
	"net/http"
	"strconv"
)

func StartServer(portByte []byte, host string, router *mux.Router) {
	portNumber, err := strconv.Atoi(string(portByte))
	if err != nil || !IsPortInUse(host, portNumber) {
		fmt.Println("You entered something that can't be used as a port. So the port was chosen for you.")
		availablePort, err := AvailablePort()
		if err != nil {
			panic(err)
		}
		inputPort := strconv.Itoa(availablePort)
		fmt.Printf("Starting server on port %s. Head over to localhost:%s to run the application.\n", inputPort, inputPort)
		log.Fatalln(http.ListenAndServe(":"+inputPort, router))
	} else {
		fmt.Printf("Starting server on port %s. Head over to localhost:%s to run the application.\n", string(portByte),
			string(portByte))
		log.Fatalln(http.ListenAndServe(":"+string(portByte), router))
	}
}
