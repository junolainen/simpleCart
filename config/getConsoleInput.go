package config

import (
	"bufio"
	"fmt"
	"os"
)

func GetConsoleInput() []byte {
	reader := bufio.NewReader(os.Stdin)
	fmt.Println("Please enter the port number you wish to use for the application. Try not to go too crazy, alright?")
	fmt.Println("If you enter a port that is already in use or a number that doesn't resemble a port at all, the port will be chosen for you.")
	fmt.Print("Port: ")
	portByte, _, _ := reader.ReadLine()
	return portByte
}
