package config

import "net/http"

func ServeFiles(w http.ResponseWriter, r *http.Request) {
	switch r.URL.Path {
	case "/css/style.css":
		http.ServeFile(w, r, "./static/css/style.css")
	case "/js/script.js":
		http.ServeFile(w, r, "./static/js/script.js")
	case "/paid.html":
		http.ServeFile(w, r, "./templates/paid.html")
	case "/error.html":
		http.ServeFile(w, r, "./templates/error.html")
	}
}
