package config

import "net"

func AvailablePort() (int, error) {
	listen, err := net.Listen("tcp", ":0")
	if err != nil {
		return 0, err
	}
	listen.Close()
	if err != nil {
		return 0, err
	}
	return listen.Addr().(*net.TCPAddr).Port, nil
}
