package config

import (
	"log"
	"strconv"
)

func AtoiNoErrors(input string) int {
	num, err := strconv.Atoi(input)
	if err != nil {
		log.Fatalln(err)
	}
	return num
}
